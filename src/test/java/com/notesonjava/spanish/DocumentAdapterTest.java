package com.notesonjava.spanish;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.assertj.core.api.Assertions;
import org.bson.BsonDateTime;
import org.bson.Document;
import org.junit.Test;

import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.VerbAnswer;

public class DocumentAdapterTest {

	
	@Test
	public void convertConjugationAnswer() {
		Conjugation c = new Conjugation();
		c.setPersona(Persona.EL);
		c.setTiempo(Tiempo.ImperativoPresente);
		c.setWord("word");
		c.setVerb("verb");
		ConjugationAnswer answer = new ConjugationAnswer();
		answer.setAnswer("answer");
		answer.setAnswerTime(ZonedDateTime.of(2018, 11, 21, 14, 30, 15, 0, ZoneOffset.UTC));
		answer.setUsername("username");
		answer.setScore(10);
		answer.setConjugation(c);
		Document doc = DocumentAdapter.toDocument(answer);
		BsonDateTime answerTime = (BsonDateTime) doc.get("answerTime");
		Date answerDate = new Date(answerTime.getValue());
		doc.replace("answerTime", answerDate);
		ConjugationAnswer result = DocumentAdapter.toConjugationAnswer(doc);
		Assertions.assertThat(result).isEqualTo(answer);	
	}
	

	@Test
	public void convertVerbAnswer() {
		VerbAnswer answer = new VerbAnswer("correr", "user");
		answer.setAnswer("correr");
		answer.setUsername("user");
		answer.setScore(10);
		answer.setAnswerTime(ZonedDateTime.of(2018, 11, 21, 14, 30, 15, 0, ZoneOffset.UTC));
		
		Document doc = DocumentAdapter.toDocument(answer);
		BsonDateTime answerTime = (BsonDateTime) doc.get("answerTime");
		Date answerDate = new Date(answerTime.getValue());
		doc.replace("answerTime", answerDate);
		
		
		VerbAnswer result = DocumentAdapter.toVerbAnswer(doc);
		Assertions.assertThat(result).isEqualTo(answer);	
		
	}
}
