package com.notesonjava.spanish;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.bson.BsonDateTime;
import org.bson.Document;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.hamcrest.MockitoHamcrest;

import com.notesonjava.spanish.conjugation.ConjugationAnswerRepository;
import com.notesonjava.spanish.conjugation.ConjugationClient;
import com.notesonjava.spanish.conjugation.ConjugationQuizzService;
import com.notesonjava.spanish.dto.ConjugationResponse;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.VerbConjugation;
import com.notesonjava.spanish.model.WordStats;
import com.notesonjava.spanish.verbs.VerbClient;

import io.reactivex.Flowable;

public class ConjugationQuizzServiceTest {
	
	private ConjugationAnswerRepository answerRepo;
	
	private ConjugationClient client;
	
	private VerbClient verbClient;
	
	private ConjugationQuizzService service;
	
	private static String USER = "user1";
	private static List<Conjugation> CONJ_LIST = new ArrayList<>();
	
	
	@BeforeClass
	public static void init(){
		for(int i=0; i<50; i++){
			Conjugation c = new Conjugation("word-"+i, Tiempo.IndicativoPresente, Persona.EL, "verb-"+i);
			CONJ_LIST.add(c);
		}		
	}
	
	@Before
	public void setup(){
		answerRepo = Mockito.mock(ConjugationAnswerRepository.class);
		client = Mockito.mock(ConjugationClient.class);
		verbClient = Mockito.mock(VerbClient.class);
		service = new ConjugationQuizzService(client, verbClient, answerRepo);
	}
	
	
	@Test
	public void test_generate_conjugations() throws IOException {
		
		List<VerbConjugation> conjugationList = new ArrayList<>();
		for(int i=0; i<100; i++){
			VerbConjugation vc = new VerbConjugation();
			vc.setTiempo(Tiempo.Gerundio);
			vc.setVerb("v"+i);
			vc.addConjugation(new Conjugation("v-"+i+"o", Tiempo.Gerundio, Persona.NONE, "v"+i));
			conjugationList.add(vc);
		}
		
		
		Mockito.when(client.getConjugations(MockitoHamcrest.argThat(Matchers.any(List.class)), MockitoHamcrest.argThat(Matchers.any(Tiempo.class)))).thenReturn(conjugationList);
		
		ConjugationResponse response = new ConjugationResponse();
		response.setConjugations(CONJ_LIST);
		
		List<Conjugation> result = service.generate(USER, Arrays.asList("verb-1", "verb-2", "verb-3"), Tiempo.IndicativoPresente, 10);
		assertThat(result).hasSize(10);
		
	}
	
	@Test
	public void test_conjugation_report_() {
		
		List<ConjugationAnswer> answers = createAnswerList();
		Flowable<Document> answerFlow = Flowable.fromIterable(answers)
				.map(answer -> DocumentAdapter.toDocument(answer))
				.doOnNext(doc ->{
					BsonDateTime answerTime = (BsonDateTime) doc.get("answerTime");
					Date answerDate = new Date(answerTime.getValue());
					doc.replace("answerTime", answerDate);	
				});
				
		Mockito.when(answerRepo.findByUsername(MockitoHamcrest.argThat(Matchers.equalTo(USER)))).thenReturn(answerFlow);
		List<WordStats<Conjugation>> result = service.conjugationReport(USER).toList().blockingGet();
		assertThat(result).hasSize(50);
		verifyReportSortedByScore(result);
	}
	
	
	
	
	private static void verifyReportSortedByScore(List<WordStats<Conjugation>> result) {
		double previousScore = 100.0;
		for(WordStats<Conjugation> w : result){
			assertThat(w.getScore()).isLessThanOrEqualTo(previousScore); 
			assertThat(w.getScore()).isGreaterThanOrEqualTo(0.0);
			previousScore = w.getScore();
			System.out.println(w.getItem().getWord() + " : "+w.getScore());
		}
	}
	
	private static List<ConjugationAnswer> createAnswerList(){
		List<ConjugationAnswer> list = new ArrayList<>();
		for(Conjugation c : CONJ_LIST){
			int total = c.getVerb().length();
			for(int i=0; i<total ; i++){
				if(i % (c.getWord().length()) == 0){
					list.add(correctAnswer(c));
				}else{
					list.add(wrongAnswer(c));
				}
			}	
		}
		return list;
	}
	
	private static ConjugationAnswer wrongAnswer(Conjugation c) {
		ConjugationAnswer answer = new ConjugationAnswer();
		answer.setConjugation(c);
		answer.setAnswer(c.getWord()+"a");
		answer.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
		return answer;
	}



	private static ConjugationAnswer correctAnswer(Conjugation c) {
		ConjugationAnswer answer = new ConjugationAnswer();
		answer.setConjugation(c);
		answer.setAnswer(c.getWord());
		answer.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
		return answer;
	}
}