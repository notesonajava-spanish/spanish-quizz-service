package com.notesonjava.spanish;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.assertj.core.api.Assertions;
import org.bson.BsonDateTime;
import org.bson.Document;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.hamcrest.MockitoHamcrest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbAnswer;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.model.WordStats;
import com.notesonjava.spanish.verbs.VerbAnswerRepository;
import com.notesonjava.spanish.verbs.VerbClient;
import com.notesonjava.spanish.verbs.VerbQuizzService;

import io.reactivex.Flowable;

public class VerbQuizzServiceTest {
	
	
	
	
	private static String USER = "user1";
	
	private static List<String> VERB_LIST = Arrays.asList("estar", "ser", "tener", "haber", "hacer", "dar", "decir", "mendar", "manejar", "disfrutar", "llover", "poner", "poder");
	
	private VerbAnswerRepository answerRepo;
	private VerbClient client;
	private VerbQuizzService service;
	
	
	@Before
	public void setup() {
		answerRepo = Mockito.mock(VerbAnswerRepository.class);
		client = Mockito.mock(VerbClient.class);
		service = new VerbQuizzService(client, answerRepo);
	}
	
	@Test
	public void test_generate() throws JsonParseException, JsonMappingException, IOException{
		
		List<Verb> verbs = new ArrayList<>();
		for(int i = 0; i< 100; i++){
			verbs.add(new Verb("myverb-"+i));
		}

		Mockito.when(client.getPopularVerbs(MockitoHamcrest.longThat(Matchers.any(Long.class)))).thenReturn(verbs);
		
		List<VerbQuestion> synonymList = new ArrayList<>();
		for(Verb v : verbs.subList(0, 10)){//10 verbs will be requested by the service.
			VerbQuestion vs = new VerbQuestion();
			vs.setVerb(v);
			synonymList.add(vs);
		}
		
		Mockito.when(answerRepo.findByUsername(MockitoHamcrest.argThat(Matchers.equalTo(USER)))).thenReturn(Flowable.fromIterable(new ArrayList<>()));
		
		Mockito.when(answerRepo.findByVerbInAndUsername(MockitoHamcrest.argThat(Matchers.any(List.class)),MockitoHamcrest.argThat(Matchers.equalTo(USER))))
			.thenReturn(Flowable.fromIterable(new ArrayList<>()));
		
		Mockito.when(client.getSynonymsFromStats(MockitoHamcrest.argThat(Matchers.any(List.class)))).thenReturn(synonymList);
		
		List<VerbQuestion> result = service.generate(USER,10);
		Assertions.assertThat(result).hasSize(10);
	}
	
	@Test
	public void test_verb_report_all_verbs() {
		List<VerbAnswer> answers = createAnswerList(10);
		Flowable<Document> answerPublisher = Flowable.fromIterable(answers)
				.map(answer -> DocumentAdapter.toDocument(answer))
				.doOnNext(doc -> {
					BsonDateTime answerTime = (BsonDateTime) doc.get("answerTime");
					Date answerDate = new Date(answerTime.getValue());
					doc.replace("answerTime", answerDate);	
				});
		Mockito.when(answerRepo.findByUsername(MockitoHamcrest.argThat(Matchers.equalTo(USER)))).thenReturn(answerPublisher);
		List<WordStats<String>> result = service.verbReport(USER).toList().blockingGet();
		Assertions.assertThat(result).hasSize(10);
		
		
		verifyReportSortedByScore(result);
	}
	
	@Test
	public void test_verb_report_with_verb_list() {
		List<VerbAnswer> answers = createAnswerList(5);
		Flowable<Document> answerPublisher = Flowable.fromIterable(answers).map(answer -> DocumentAdapter.toDocument(answer));

		Mockito.when(answerRepo.findByVerbInAndUsername(MockitoHamcrest.argThat(Matchers.any(List.class)),MockitoHamcrest.argThat(Matchers.equalTo(USER))))
				.thenReturn(Flowable.fromIterable(new ArrayList<>()));
	
		
		Mockito.when(answerRepo.findByUsername(MockitoHamcrest.argThat(Matchers.equalTo(USER)))).thenReturn(answerPublisher);
		
		
		List<WordStats<String>> result = service.verbReport(VERB_LIST.subList(0, 5), USER).toList().blockingGet();
		Assertions.assertThat(result).hasSize(5);
		verifyReportSortedByScore(result);

	}
	
	@Test
	public void test_daily_report_without_data(){

		
		Flowable<Document> answerPublisher = Flowable.fromIterable(new ArrayList<Document>());
		
		
		Mockito.when(answerRepo.findByAnswerTimeAfterAndUsername(MockitoHamcrest.argThat(Matchers.any(ZonedDateTime.class)), MockitoHamcrest.argThat(Matchers.equalTo(USER))))
					.thenReturn(answerPublisher);
		
		List<WordStats<LocalDate>> report = service.getDailyReport(10, USER).toList().blockingGet();
		Assertions.assertThat(report).hasSize(0);
	}
	
	
	private void verifyReportSortedByScore(List<WordStats<String>> result) {
		double previousScore = 100.0;
		for(WordStats<String> w : result){
			Assertions.assertThat(w.getScore()).isLessThanOrEqualTo(previousScore); 
			Assertions.assertThat(w.getScore()).isGreaterThanOrEqualTo(0.0);
			previousScore = w.getScore();
			System.out.println(w.getItem() + " : "+w.getScore());
		}
		
	}
	
	private List<VerbAnswer> createAnswerList(int size){
		List<VerbAnswer> list = new ArrayList<>();
		for(String s : VERB_LIST.subList(0, size)){
			int total = s.length();
			for(int i=0; i<total ; i++){
				if(i % (s.length()) == 0){
					list.add(correctAnswer(s));
				}else{
					list.add(wrongAnswer(s));
				}
			}
		}
		return list;
	}

	private static VerbAnswer wrongAnswer(String verb) {
		VerbAnswer answer = new VerbAnswer();
		answer.setVerb(verb);
		answer.setAnswer(verb+"a");
		answer.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
		
		return answer;
	}

	private static VerbAnswer correctAnswer(String verb) {
		VerbAnswer answer = new VerbAnswer();
		answer.setVerb(verb);
		answer.setAnswer(verb);
		answer.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
		return answer;
	}
}
