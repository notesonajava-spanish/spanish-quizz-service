package com.notesonjava.spanish;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.model.WordStats;
import com.notesonjava.spanish.verbs.VerbClient;
import com.notesonjava.spanish.verbs.VerbUrlBuilder;



public class VerbClientTest {


	@Rule
	public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

	
	private VerbUrlBuilder urlBuilder;
	private VerbClient client;
	
	@Before
	public void init() {
		urlBuilder = new VerbUrlBuilder(wireMockRule.baseUrl());
		ObjectMapper mapper = new ObjectMapper();
		client = new VerbClient(urlBuilder, mapper);
	
	}

	
	
	@Test
	public void testSingleMappingWithoutTranslations() throws JsonParseException, JsonMappingException, IOException {
		
		String msg = "{ \"spanish\": \"ir\", \"regular\": false, \"views\": 1112016 }";
		
		ObjectMapper mapper = new ObjectMapper();
		Verb v = mapper.readValue(msg, Verb.class);
		Assertions.assertThat(v).isNotNull();
	}

	
	@Test
	public void testSingleMapping() throws JsonParseException, JsonMappingException, IOException {
		String msg = "{ \"spanish\": \"ir\", \"translations\": { \"ENGLISH\":  [ \"go\" ], \"FRENCH\": [ \"aller\" ] }, \"regular\": false, \"views\": 1112016 }";	
		ObjectMapper mapper = new ObjectMapper();
		Verb v = mapper.readValue(msg, Verb.class);
		Assertions.assertThat(v).isNotNull();
	}
	
	@Test
	public void testMapping() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		try(InputStream is = VerbClientTest.class.getClassLoader().getResourceAsStream("popularVerbs.json")){
			Verb[] verbs = mapper.readValue(is, Verb[].class);
			List<Verb> verbList = Arrays.asList(verbs);
			Assertions.assertThat(verbList).hasSize(200);
		}	
	}
	
	@Test
	public void test_get_verbs() throws IOException{
		
		List<Verb> verbs = new ArrayList<>();
		for(int i = 0; i< 100; i++){
			verbs.add(new Verb("myverb-"+i));
		}

		
		ObjectMapper mapper = new ObjectMapper();
		String verbBody = mapper.writeValueAsString(verbs);
		String url = urlBuilder.popularVerbs(30);

		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(url.substring(wireMockRule.baseUrl().length())))
	            .willReturn(WireMock.aResponse()
	                .withHeader("Content-Type", "application/json")
	                .withBody(verbBody)));


		
		
		List<Verb> result = client.getPopularVerbs(30);
		Assertions.assertThat(result).isEqualTo(verbs);
	}
	
	@Test
	public void test_get_synonyms() throws IOException{
		

		List<Verb> verbs = new ArrayList<>();
		for(int i = 0; i< 20; i++){
			verbs.add(new Verb("myverb-"+i));
		}

		List<WordStats<String>> stats = verbs.stream().map(v -> new WordStats<>(v.getSpanish())).collect(Collectors.toList());

		String synonymUrl =urlBuilder.synonymsFromVerbs(verbs);


		List<VerbQuestion> synonymList = new ArrayList<>();
		for(Verb v : verbs.subList(0, 10)){//10 verbs will be requested by the service.
			VerbQuestion vs = new VerbQuestion();
			vs.setVerb(v);
			synonymList.add(vs);
		}

		ObjectMapper mapper = new ObjectMapper();
		String synonymBody = mapper.writeValueAsString(synonymList);

		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(synonymUrl.substring(wireMockRule.baseUrl().length())))
	            .willReturn(WireMock.aResponse()
	                .withHeader("Content-Type", "application/json")
	                .withBody(synonymBody)));
		
		
		List<VerbQuestion> result = client.getSynonymsFromStats(stats);
		Assertions.assertThat(result).isEqualTo(synonymList);
	}
	
	
	@Test
	public void test_get_by_suffixes() throws IOException{

		List<Verb> verbs = new ArrayList<>();
		for(int i = 0; i< 10; i++){
			verbs.add(new Verb("myverb-"+i));
		}
		
		String suffixUrl =urlBuilder.suffixes("enar", "anar");
		System.out.println(suffixUrl);
		
		ObjectMapper mapper = new ObjectMapper();
		String suffixBody = mapper.writeValueAsString(verbs.subList(0, 10));
		System.out.println(suffixBody);

		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(suffixUrl.substring(wireMockRule.baseUrl().length())))
	            .willReturn(WireMock.aResponse()
	                .withHeader("Content-Type", "application/json")
	                .withBody(suffixBody)));
		
		List<Verb> result = client.getBySuffixes("enar", "anar");
		Assertions.assertThat(result).isEqualTo(verbs);
	}

	@Test
	public void test_get_by_suffixes_with_accent() throws IOException{

		List<Verb> verbs = new ArrayList<>();
		for(int i = 0; i< 10; i++){
			verbs.add(new Verb("myverb-"+i));
		}
		
		String suffixUrl =urlBuilder.suffixes("eñar");
		System.out.println(suffixUrl);
		
		ObjectMapper mapper = new ObjectMapper();
		String suffixBody = mapper.writeValueAsString(verbs.subList(0, 10));
		System.out.println(suffixBody);

		WireMock.stubFor(WireMock.get(WireMock.urlEqualTo(suffixUrl.substring(wireMockRule.baseUrl().length())))
	            .willReturn(WireMock.aResponse()
	                .withHeader("Content-Type", "application/json")
	                .withBody(suffixBody)));
		
		client.getBySuffixes("eñar").forEach(System.out::println);;
		//List<Verb> result = client.getBySuffixes("eñar");
		//Assertions.assertThat(result).isEqualTo(verbs);
	}
	
}
