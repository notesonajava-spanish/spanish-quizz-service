package com.notesonjava.spanish;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.bson.Document;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.commands.CommandResult;
import com.notesonjava.commands.compose.DockerComposeCommands;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.model.VerbAnswer;
import com.notesonjava.spanish.verbs.VerbAnswerRepository;

import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VerbAnswerRepositoryTest {

	
private static MongoClient mongoClient;
	
	private static boolean useCompose;
	
	private static VerbAnswerRepository repo;
	
	private static final String user1 = "student1";
	
	private static final String user2 = "student2";
	
	List<String> verbList = Arrays.asList("decir", "hacer", "ser", "estar"); 

	
	@BeforeClass
	public static void init() throws IOException {
		
		PropertyMap props = PropertyLoader.loadProperties();
		
		String dbHost = props.get("db.host").orElse("localhost");
		int dbPort = Integer.parseInt(props.get("db.port").orElse("27017"));
		
		useCompose = Boolean.parseBoolean(props.get("use.compose").orElse("true"));
		
		
		if(useCompose) {
			CommandResult composeResult = DockerComposeCommands.up("docker-compose-test.yml");
			
			if(composeResult != null) {
				composeResult.getOutput().forEach(System.out::println);
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		mongoClient = MongoClients.create(QuizzApplication.localConnectionString(dbHost, dbPort));
		
		MongoDatabase database = mongoClient.getDatabase("answers");
		repo = new VerbAnswerRepository(database);
	
	}
	
	@AfterClass
	public static void close() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mongoClient.close();
		if(useCompose) {
			DockerComposeCommands.down("docker-compose-test.yml");			
		}
	
	}
	
	@Before
	public void setup(){
		
		for(int i=1; i<= 10; i++){
			ZonedDateTime answerTime = ZonedDateTime.now();
			answerTime = answerTime.minusDays(i);
			for(String s : verbList){
				createAnswer(s, answerTime, user1);
				createAnswer(s, answerTime, user2);
			}
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@After
	public void after() {
		EditingSubscriber subscriber = new EditingSubscriber();
		repo.deleteAll().subscribe(subscriber);
		
		
	}
	
	private void createAnswer(String verb, ZonedDateTime answerTime, String username) {
		VerbAnswer answer = new VerbAnswer(verb, username);
		answer.setAnswer(verb+"_");
		answer.setAnswerTime(answerTime);
		Document doc = DocumentAdapter.toDocument(answer);
		log.info("Create : " + doc.toString());
		repo.save(doc);
	}
	
	@Test
	public void test_find_answers_by_verb(){
		List<Document> answers = Flowable.fromPublisher(repo.findByVerb("hacer"))
				.doOnNext(doc -> log.info("Retrieved : " + doc.toString()))
				.toList().blockingGet();
		
		/*List<VerbAnswer> answers = Flowable.fromPublisher(repo.findByVerb("hacer"))
				.doOnNext(doc -> log.info("Retrieved : " + doc.toString()))
				.map(doc -> DocumentAdapter.toVerbAnswer(doc))
				.toList().blockingGet();*/
		Assertions.assertThat(answers).hasSize(20);
	}
	
	@Test
	public void test_find_answers_by_and_username(){
		
		List<VerbAnswer> answers = Flowable.fromPublisher(repo.findByUsername(user1)).map(doc -> DocumentAdapter.toVerbAnswer(doc)).toList().blockingGet();
		Assertions.assertThat(answers).hasSize(40);
	}
	
	@Test
	public void test_find_answers_by_verb_and_user(){
		List<VerbAnswer> answers = Flowable.fromPublisher(repo.findByVerbAndUsername("hacer", user1)).map(doc -> DocumentAdapter.toVerbAnswer(doc)).toList().blockingGet();
		Assertions.assertThat(answers).hasSize(10);
	}
	

	@Test
	public void test_find_answers_after_by_user(){
		
		ZonedDateTime answerTime = ZonedDateTime.now();
		answerTime = answerTime.minusDays(5);
		
		List<VerbAnswer> answers = Flowable.fromPublisher(repo.findByAnswerTimeAfterAndUsername(answerTime, user1)).map(doc -> DocumentAdapter.toVerbAnswer(doc)).toList().blockingGet();
		Assertions.assertThat(answers).hasSize(16);
	}
	
	@Test
	public void test_find_by_verb_and_user(){
		List<VerbAnswer> answers = Flowable.fromPublisher(repo.findByVerbAndUsername("hacer", user1)).map(doc -> DocumentAdapter.toVerbAnswer(doc)).toList().blockingGet();
		Assertions.assertThat(answers).hasSize(10);
	}
	
	@Test
	public void test_find_by_verb_list_and_user(){
		List<VerbAnswer> answers = Flowable.fromPublisher(repo.findByVerbInAndUsername(Arrays.asList("hacer", "creer", "cruzar", "llover", "poner"), user1)).map(doc -> DocumentAdapter.toVerbAnswer(doc)).toList().blockingGet();
		Assertions.assertThat(answers).hasSize(10);
	}

}