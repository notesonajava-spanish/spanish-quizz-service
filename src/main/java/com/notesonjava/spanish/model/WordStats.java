package com.notesonjava.spanish.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor
public class WordStats<T> {

	@NonNull
	private T item;
	private int validAnswers;
	private int totalAnswers;
	
	public void addValid(){
		validAnswers++;
	}
	
	public void addAttempt(){
		totalAnswers++;
	}
	
	@JsonIgnore
	public double getPercent(){
		if(totalAnswers == 0){
			return 0;
		}
		return ((double)validAnswers / (double)totalAnswers) * 100;
	}
	
	public double getScore(){
		return ((double)validAnswers / (double)(totalAnswers+1)) * 100;
	}
}