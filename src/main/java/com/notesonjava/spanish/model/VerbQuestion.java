package com.notesonjava.spanish.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Data;

@Data
public class VerbQuestion {

	private Verb verb;
	private List<Verb> synonyms = new ArrayList<>();
	private Set<String> acceptedAnswers = new HashSet<>();
	
	public void addSynonym(Verb v){
		synonyms.add(v);
	}
	
	public void addSynonyms(List<Verb> list){
		synonyms.addAll(list);
	}
	
	public void addAcceptedAnswer(String answer){
		acceptedAnswers.add(answer);
	}
	
	public void addAcceptedAnswers(List<String> answers){
		acceptedAnswers.addAll(answers);
	}
}