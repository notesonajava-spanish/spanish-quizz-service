package com.notesonjava.spanish.model;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import lombok.Data;

@Data
public abstract class AbstractAnswer {
	
	private String username;
	
	private String answer;
	
	private ZonedDateTime answerTime;
	
	private int score;
	
	public LocalDate getAnswerDay(){
		return answerTime.toLocalDate();
	}
}