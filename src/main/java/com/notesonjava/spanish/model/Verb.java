package com.notesonjava.spanish.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonIgnoreProperties({"regular", "views"})
public class Verb{

	private String spanish;
	
	private Map<String, Set<String>> translations = new HashMap<>();
	
	public Verb(String spanish){
		this.spanish = spanish;
	}	
}

/*

[
{"verb":{"spanish":"enviar","translations":{"en":["send"],"fr":["envoyer"]},"regular":false,"views":13738},"synonyms":[{"spanish":"expedir","translations":{"en":["dispatch","issue","send"]},"regular":false,"views":1401}]},
{"verb":null,"synonyms":[]},
{"verb":{"spanish":"compartir","translations":{"en":["share"],"fr":["partager"]},"regular":true,"views":3588},"synonyms":[]},
{"verb":{"spanish":"navegar","translations":{"en":["surf (the internet)","sail"],"fr":["naviguer"]},"regular":false,"views":3277},"synonyms":[]},{"verb":{"spanish":"abrir","translations":{"en":["open"],"fr":["ouvrir"]},"regular":false,"views":18717},"synonyms":[{"spanish":"eclosionar","translations":{"en":["hatch","open"]},"regular":true,"views":1743}]},{"verb":{"spanish":"exigir","translations":{"en":["require","demand"],"fr":["exiger"]},"regular":false,"views":3360},"synonyms":[{"spanish":"reclamar","translations":{"en":["claim","demand"]},"regular":true,"views":1692}]},{"verb":{"spanish":"levantar","translations":{"en":["raise"],"fr":["Ã©lever"]},"regular":true,"views":5009},"synonyms":[{"spanish":"elevar","translations":{"en":["raise"]},"regular":true,"views":1721},{"spanish":"alzar","translations":{"en":["raise","lift","hoist"]},"regular":false,"views":1517},{"spanish":"plantear","translations":{"en":["raise"]},"regular":true,"views":1323}]},{"verb":{"spanish":"firmar","translations":{"en":["sign"]},"regular":true,"views":12622},"synonyms":[]},{"verb":{"spanish":"describir","translations":{"en":["describe"]},"regular":false,"views":12097},"synonyms":[]},{"verb":{"spanish":"practicar","translations":{"en":["practise"],"fr":["pratiquer"]},"regular":false,"views":4159},"synonyms":[{"spanish":"ejercer","translations":{"en":["practise","exert","exercise"]},"regular":false,"views":2207},{"spanish":"ensayar","translations":{"en":["practise"]},"regular":true,"views":1456}]}]
VerbQuestion(verb=Verb(spanish=enviar, translations={en=[send], fr=[envoyer]}), synonyms=[Verb(spanish=expedir, translations={en=[dispatch, issue, send]})], acceptedAnswers=[])
VerbQuestion(verb=null, synonyms=[], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=compartir, translations={en=[share], fr=[partager]}), synonyms=[], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=navegar, translations={en=[surf (the internet), sail], fr=[naviguer]}), synonyms=[], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=abrir, translations={en=[open], fr=[ouvrir]}), synonyms=[Verb(spanish=eclosionar, translations={en=[hatch, open]})], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=exigir, translations={en=[require, demand], fr=[exiger]}), synonyms=[Verb(spanish=reclamar, translations={en=[claim, demand]})], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=levantar, translations={en=[raise], fr=[Ã©lever]}), synonyms=[Verb(spanish=elevar, translations={en=[raise]}), Verb(spanish=alzar, translations={en=[raise, lift, hoist]}), Verb(spanish=plantear, translations={en=[raise]})], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=firmar, translations={en=[sign]}), synonyms=[], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=describir, translations={en=[describe]}), synonyms=[], acceptedAnswers=[])
VerbQuestion(verb=Verb(spanish=practicar, translations={en=[practise], fr=[pratiquer]}), synonyms=[Verb(spanish=ejercer, translations={en=[practise, exert, exercise]}), Verb(spanish=ensayar, translations={en=[practise]})], acceptedAnswers=[])


*/