package com.notesonjava.spanish.model;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString(callSuper=true)
@EqualsAndHashCode(callSuper=true)
public class VerbAnswer extends AbstractAnswer {
	
	private String verb;
	
	public VerbAnswer(String spanish, String username){
		this.verb = spanish;
		this.setUsername(username);
		this.setAnswerTime(ZonedDateTime.now(ZoneOffset.UTC));
	}
	
}