package com.notesonjava.spanish.dto;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenericAnswer<T> {

	private String answer;
	private ZonedDateTime answerTime;
	private T question;
	private int score;
	
	public GenericAnswer(T question){
		this.question = question;
		answerTime = ZonedDateTime.now(ZoneOffset.UTC);
	}	
}