package com.notesonjava.spanish.dto;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class VerbAnswerDto extends GenericAnswer<String> {

}
