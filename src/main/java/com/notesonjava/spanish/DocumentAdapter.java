package com.notesonjava.spanish;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.bson.BsonDateTime;
import org.bson.Document;

import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.VerbAnswer;

public class DocumentAdapter {
	
	public static Document toDocument(ConjugationAnswer answer) {
		Document doc = new Document();
		doc.put("conjugation", toDocument(answer.getConjugation()));
		doc.put("answer", answer.getAnswer());
		doc.put("username", answer.getUsername());
		doc.put("score", answer.getScore());
		doc.put("answerTime", new BsonDateTime(answer.getAnswerTime().toInstant().toEpochMilli()));
		return doc;
	}
	
	public static Document toDocument(Conjugation c) {
		Document doc = new Document();
		doc.put("word", c.getWord());
		doc.put("tiempo", c.getTiempo().toString());
		doc.put("persona", c.getPersona().toString());
		doc.put("verb", c.getVerb());
		return doc;
	}
	
	public static ConjugationAnswer toConjugationAnswer(Document doc) {
		ConjugationAnswer answer = new ConjugationAnswer();
		answer.setAnswer(doc.getString("answer"));
		Object obj = doc.get("conjugation");
		Document conjugationDoc = (Document) obj;
		answer.setConjugation(toConjugation(conjugationDoc));
		answer.setUsername(doc.getString("username"));
		answer.setScore(doc.getInteger("score", 0));
		Date answerDate = doc.getDate("answerTime");
		Instant instant = Instant.ofEpochMilli(answerDate.getTime());
		answer.setAnswerTime(ZonedDateTime.ofInstant(instant, ZoneOffset.UTC));
		return answer;
	}
	
	public static Conjugation toConjugation(Document doc) {
		Conjugation c = new Conjugation();
		c.setPersona(Persona.valueOf(doc.getString("persona")));
		c.setTiempo(Tiempo.valueOf(doc.getString("tiempo")));
		c.setWord(doc.getString("word"));
		c.setVerb(doc.getString("verb"));
		return c;
	}
	
	public static Document toDocument(VerbAnswer answer) {
		Document doc = new Document();
		doc.put("verb", answer.getVerb());
		doc.put("answer", answer.getAnswer());
		doc.put("username", answer.getUsername());
		doc.put("score", answer.getScore());
		doc.put("answerTime", new BsonDateTime(answer.getAnswerTime().toInstant().toEpochMilli()));
		return doc;
	}

	public static VerbAnswer toVerbAnswer(Document doc) {
		VerbAnswer answer = new VerbAnswer(doc.getString("verb"), doc.getString("username"));
		answer.setAnswer(doc.getString("answer"));
		answer.setScore(doc.getInteger("score",0));
		Date answerDate = doc.getDate("answerTime");
		Instant instant = Instant.ofEpochMilli(answerDate.getTime());
		answer.setAnswerTime(ZonedDateTime.ofInstant(instant, ZoneOffset.UTC));
		return answer;	
	}
}
