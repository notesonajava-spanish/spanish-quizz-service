package com.notesonjava.spanish.verbs;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.WordStats;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbUrlBuilder {
	private String verbUrl;


	public String popularVerbs(){
		return verbUrl+"/popular";
	}
	
	public String popularVerbs(long end){
		return verbUrl+"/popular?end="+end;
	}
	
	public String suffixes(String...suffixes) {
		StringBuilder url = new StringBuilder(verbUrl);
		url.append("/suffix?");
		List<String> list = Arrays.asList(suffixes);
		for(String s : list){			
			try {
				url.append("suffix=").append(URLEncoder.encode(s, "UTF-8")).append("&");
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to encode the suffix for url : " + s, e);
			}
		}
		if(!list.isEmpty()){
			return url.substring(0, url.length()-1);
		}
		return url.toString();
	}
	
	public String synonymsFromStats(List<WordStats<String>> list){
		
		StringBuilder url = new StringBuilder(verbUrl);
		url.append("/synonyms?");
		
		for(WordStats<String> vs : list){			
			url.append("verb=").append(vs.getItem()).append("&");
		}
		if(!list.isEmpty()){
			return url.substring(0, url.length()-1);
		}
		return url.toString();
	}
	
	public String synonymsFromVerbs(List<Verb> list){
		
		StringBuilder url = new StringBuilder(verbUrl);
		url.append("/synonyms?");
		
		for(Verb v : list){			
			url.append("verb=").append(v.getSpanish()).append("&");
		}
		if(!list.isEmpty()){
			return url.substring(0, url.length()-1);
		}
		return url.toString();
}
	
}
