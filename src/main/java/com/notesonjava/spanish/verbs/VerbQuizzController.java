package com.notesonjava.spanish.verbs;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.auth.UserHandler;
import com.notesonjava.spanish.dto.VerbAnswerDto;
import com.notesonjava.spanish.model.VerbAnswer;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.model.WordStats;

import io.javalin.Javalin;
import jodd.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbQuizzController {
	
	private VerbQuizzService verbService;
	private VerbAnswerRepository verbRepo;
	private UserHandler userHandler;
	
	
	
	public void init(Javalin app) {
		
		app.after(ctx -> {
			//log.info("After Response : " + ctx.resultString() + ", " + ctx.status());
		});
		
		app.get("/verbs/ping", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User: " + user.get());
			} else {
				log.info("Missing user");
			}
			ctx.result("I am alive : " +user.orElse("Missing User"));
		});
		
		
		app.get("/verbs", ctx -> {
			log.info("Create quizz");
		//	ctx.contentType(APPLICATION_JSON_UTF8);
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("Retrieved user ");
				log.info(user.toString());
				
				List<VerbQuestion> list = verbService.generate(user.get(), 10);
				list.forEach(System.out::println);
				list.forEach(q ->{
					log.debug("Create question from " + q);
					q.addAcceptedAnswer(q.getVerb().getSpanish());
					q.addAcceptedAnswer(StringUtils.stripAccents(q.getVerb().getSpanish()));
				});
				
				ctx.status(200);
				ctx.json(list);
			} else {
				log.error("Missing user");
			}
				
		});
		
		app.post("/verbs/answers", ctx -> {
			log.info("Begin save");
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User : " + user.get());
				VerbAnswerDto[] answerBody = ctx.bodyAsClass(VerbAnswerDto[].class);
				List<VerbAnswerDto> answers = Arrays.asList(answerBody);
				ctx.status(HttpStatus.HTTP_CREATED);
			
				for(VerbAnswerDto a : answers){
					
					VerbAnswer answer = new VerbAnswer(a.getQuestion(),user.get());
					answer.setAnswer(a.getAnswer());
					answer.setAnswerTime(a.getAnswerTime());
					answer.setUsername(user.get());
					answer.setScore(a.getScore());
					Document doc = DocumentAdapter.toDocument(answer);
					verbRepo.save(doc);	
				}
			}
		});
			
		app.get("/verbs/report", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				List<WordStats<String>> report = verbService.verbReport(user.get()).toSortedList((s1, s2) -> (int)(s2.getScore() - s1.getScore())).blockingGet();
				ctx.json(report);
			}
		});
		
		app.get("/verbs/report/daily", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
		
				String daysParam = ctx.queryParam("numDays", "7");
				int numDays = Integer.parseInt(daysParam);
				List<WordStats<LocalDate>> result = verbService.getDailyReport(numDays, user.get()).toSortedList((s1, s2) -> (int)(s2.getScore() - s1.getScore())).blockingGet();
				ctx.status(200);
				ctx.json(result);						
			}
			
		});
		
		app.post("/verbs/report", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				String[] verbs = ctx.bodyAsClass(String[].class);
				List<String> verbList = Arrays.asList(verbs);
				ctx.status(200);
				ctx.json(verbService.verbReport(verbList, user.get()).toSortedList((s1, s2) -> (int)(s2.getScore() - s1.getScore())).blockingGet());
			}
		});
	}

}
