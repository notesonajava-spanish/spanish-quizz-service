package com.notesonjava.spanish.verbs;

import java.time.ZonedDateTime;
import java.util.List;

import org.bson.BsonDateTime;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.reactivestreams.Publisher;

import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.mongodb.reactivestreams.client.Success;
import com.notesonjava.spanish.EditingSubscriber;

import io.reactivex.Flowable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbAnswerRepository {
	private static final String COLLECTION_NAME = "verb_answers";

	private MongoDatabase database;
	
	public Flowable<Document> findByUsername(String username){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		Bson filter = Filters.eq("username", username);
		return Flowable.fromPublisher(collection.find(filter));			
	}
	
	public Publisher<Document> findByVerb(String verb){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		Bson filter = Filters.eq("verb", verb);
		return collection.find(filter);
	}
	
	public Publisher<Document> findByVerbAndUsername(String verb, String username){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		
		Bson filter = Filters.and(Filters.eq("verb", verb), Filters.eq("username", username));
		return collection.find(filter);			
	}
	
	public Publisher<Document> findByVerbInAndUsername(List<String> verbs, String username){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		Bson filter = Filters.and(Filters.in("verb", verbs), Filters.eq("username", username));
		return collection.find(filter);	
	}
	
	public Publisher<Document> findByAnswerTimeAfterAndUsername(ZonedDateTime time, String username){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		BsonDateTime refTime = new BsonDateTime(time.toInstant().toEpochMilli());
		Bson filter = Filters.and(Filters.eq("username", username), Filters.gte("answerTime", refTime));
		return collection.find(filter);	
	}
	
	public Publisher<Success> save(Document answer) {
		log.info("Save single answer: " + answer);
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		log.info(answer.toJson());
		Publisher<Success> publisher =  collection.insertOne(answer);
		publisher.subscribe(new EditingSubscriber());
		return publisher;
	}

	public Publisher<Success> deleteAll(){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		return collection.drop();
	}
	
}
