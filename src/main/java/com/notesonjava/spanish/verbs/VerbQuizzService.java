package com.notesonjava.spanish.verbs;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbAnswer;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.model.WordStats;

import io.reactivex.Flowable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbQuizzService {

	private VerbClient verbClient;
	
	private VerbAnswerRepository answerRepo;
	
	public List<VerbQuestion> generate(String username, int size) throws JsonParseException, JsonMappingException, IOException{
		List<WordStats<String>> report = verbReport(username).toList().blockingGet();
		long goodVerbs = report.stream().filter(stat -> stat.getScore() > 60).count();
		List<Verb> verbList = verbClient.getPopularVerbs(goodVerbs+20);
		List<WordStats<String>> selected = generateVerbList(username, verbList, size);
		return verbClient.getSynonymsFromStats(selected);
	}

	private List<WordStats<String>> generateVerbList(String username, List<Verb> verbList, int size) {
		List<String> nameList = Flowable.fromIterable(verbList).map(v -> v.getSpanish()).toList().blockingGet();
		List<WordStats<String>> stats = verbReport(nameList, username).toList().blockingGet();
		Collections.shuffle(stats);
		if(stats.size() <= 10) {
			return stats;
		}else {
			return stats.subList(stats.size()-10, stats.size());
		}	
	}
	
	public Flowable<WordStats<String>> verbReport(String username){
		 
		Map<String, Collection<VerbAnswer>> map = Flowable.fromPublisher(answerRepo.findByUsername(username))
				.doOnNext(doc -> log.info(doc.toString()))
				.map(doc -> DocumentAdapter.toVerbAnswer(doc))
					.doOnNext(answer -> log.info(answer.toString()))
					.toMultimap(VerbAnswer::getVerb)
					.blockingGet();
		if(log.isDebugEnabled()) {
			map.values().forEach(System.out::println);
		}
		return Flowable.fromIterable(map.entrySet()).map(entry -> createWordStats(entry.getKey(), entry.getValue()));
	}
	
	
	public Flowable<WordStats<String>> verbReport(List<String> verbs, String username){
		
		Map<String, Collection<VerbAnswer>> map = Flowable.fromPublisher(answerRepo.findByVerbInAndUsername(verbs,username))
					.map(doc -> DocumentAdapter.toVerbAnswer(doc))
					.toMultimap(VerbAnswer::getVerb)
					.blockingGet();

		return Flowable.fromIterable(verbs)
				.map(verb ->  createWordStats(verb, map.getOrDefault(verb, new ArrayList<>())));
	}
	
	public Flowable<WordStats<LocalDate>> getDailyReport(int numDays, String username) {
		
		ZonedDateTime refDate= ZonedDateTime.now().truncatedTo(ChronoUnit.DAYS).minusDays(numDays);
		
		Map<LocalDate, Collection<VerbAnswer>> map = Flowable.fromPublisher(answerRepo.findByAnswerTimeAfterAndUsername(refDate, username))
				.map(doc -> DocumentAdapter.toVerbAnswer(doc))
				.toMultimap(VerbAnswer::getAnswerDay)
				.blockingGet();
		
		return Flowable.fromIterable(map.entrySet())
				.map(entry ->  createDailyStats(entry.getKey(), entry.getValue()));
	}
	
	private WordStats<LocalDate> createDailyStats(LocalDate day, Collection<VerbAnswer> answers) {
		WordStats<LocalDate> stats = new WordStats<>();
		stats.setItem(day);
		answers.forEach(va -> {
			stats.addAttempt();
			if(va.getScore() == 1) {
				stats.addValid();
			}
		});
		return stats;
	}
	
	private WordStats<String> createWordStats(String word, Collection<VerbAnswer> answers) {
		WordStats<String> stats = new WordStats<>();
		stats.setItem(word);
		answers.forEach(va -> {
			stats.addAttempt();
			if(va.getScore() == 1) {
				stats.addValid();
			}
		});
		return stats;
	}
}
