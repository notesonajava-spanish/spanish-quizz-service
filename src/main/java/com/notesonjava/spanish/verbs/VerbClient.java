package com.notesonjava.spanish.verbs;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.model.WordStats;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbClient {

	private VerbUrlBuilder urlBuilder;

	private ObjectMapper mapper;
	
	
	public List<Verb> getPopularVerbs() throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.popularVerbs();
		HttpResponse response = HttpRequest.get(url).send();
		Verb[] verbs = mapper.readValue(response.bodyBytes(), Verb[].class);
		return Arrays.asList(verbs);
		
	}
	
	public List<Verb> getPopularVerbs(long end) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.popularVerbs(end);
		HttpResponse response = HttpRequest.get(url).send();
		Verb[] verbs = mapper.readValue(response.bodyBytes(), Verb[].class);
		return Arrays.asList(verbs);
		
	}
	
	public List<Verb> getBySuffixes(String...suffixes) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.suffixes(suffixes);
		log.info("Call url : " + url);
		HttpResponse response = HttpRequest.get(url).send();
		Verb[] verbs = mapper.readValue(response.bodyBytes(), Verb[].class);
		return Arrays.asList(verbs);
		
	}
	
	public List<VerbQuestion> getSynonymsFromStats(List<WordStats<String>> list) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.synonymsFromStats(list);
		log.info("Call : " + url);
		
		HttpResponse response = HttpRequest.get(url).send();
		VerbQuestion[] questions = mapper.readValue(response.bodyBytes(), VerbQuestion[].class);
		return Arrays.asList(questions);
	}
	
}
