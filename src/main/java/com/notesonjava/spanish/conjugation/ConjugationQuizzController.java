package com.notesonjava.spanish.conjugation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.auth.UserHandler;
import com.notesonjava.spanish.dto.ConjugationAnswerDto;
import com.notesonjava.spanish.dto.ConjugationQuestion;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.WordStats;

import io.javalin.Javalin;
import jodd.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class ConjugationQuizzController {

	private ConjugationQuizzService conjugationService;
	private ConjugationAnswerRepository conjugationRepo;
	private ObjectMapper mapper;
	
	
	private UserHandler userHandler;
	
	private static final String APPLICATION_JSON_UTF8 = "application/json;charset=utf-8";
	
	public void init(Javalin app) {
		app.get("/conjugations", ctx ->{
			Optional<String> user = userHandler.retrieveUser(ctx);
			ctx.contentType(APPLICATION_JSON_UTF8);
			
			if(user.isPresent()) {
				
				List<Conjugation> conjugationList = conjugationService.generateRandom(user.get(), 10);
				List<ConjugationQuestion> questions = new ArrayList<>();
				conjugationList.forEach(c -> {
					ConjugationQuestion question = new ConjugationQuestion();
					question.setConjugation(c);
					question.addAcceptedAnswer(c.getWord());
					question.addAcceptedAnswer(StringUtils.stripAccents(c.getWord())); 
					questions.add(question);
				});
				
				String json = mapper.writeValueAsString(questions);
				ctx.status(HttpStatus.HTTP_OK);
				ctx.result(json);
			}
		});
		
		app.post("/conjugations/answers", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				ConjugationAnswerDto[] answers =  mapper.readValue(ctx.body(), ConjugationAnswerDto[].class);
				for(ConjugationAnswerDto answer : answers){
					ConjugationAnswer a = new ConjugationAnswer(answer.getQuestion(), user.get());
					a.setAnswer(answer.getAnswer());
					a.setAnswerTime(answer.getAnswerTime());
					a.setScore(answer.getScore());
					conjugationRepo.save(DocumentAdapter.toDocument(a));
				}
				ctx.status(HttpStatus.HTTP_CREATED);
			}
		});

		app.get("/conjugations/report", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				List<WordStats<Conjugation>> result = conjugationService.conjugationReport(user.get()).toList().blockingGet();
				ctx.status(200);
				log.info("Result : " + result.toString());
				ctx.result(mapper.writeValueAsString(result));
			}
		});
		
		app.get("/conjugations/report/daily", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				List<WordStats<LocalDate>> result = conjugationService.getLastWeekReport(user.get()).toList().blockingGet();
				ctx.status(200);
				ctx.result(mapper.writeValueAsString(result));	
			}
		});
		
		app.get("/conjugations/report/tiempos", ctx ->{
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {	
				List<WordStats<Tiempo>> result = conjugationService.conjugationTiempoReport(user.get()).toList().blockingGet();
				ctx.status(200);
				ctx.result(mapper.writeValueAsString(result));
			}
		});

	}
}
