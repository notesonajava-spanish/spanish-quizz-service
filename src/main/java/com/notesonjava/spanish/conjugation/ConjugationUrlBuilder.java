package com.notesonjava.spanish.conjugation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import com.notesonjava.spanish.model.Tiempo;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ConjugationUrlBuilder {
	
	private String baseUrl;
	
	
	public String searchConjugation(){
		return baseUrl+"/search";
	}
	
	public String conjugation(List<String> verbs, Tiempo tiempo){
		StringBuilder url = new StringBuilder(baseUrl);
		url.append("/search?");
		for(String s : verbs){
			url.append("verb=").append(s).append("&");
		}
		url.append("tiempo=").append(tiempo.getValue());
		return url.toString();	
	}
	
	public String search(String... verbs) {
        String query = buildQueryString(Arrays.asList(verbs), new ArrayList<>());
        return baseUrl + "/search?" + query;
    }

    public String search(Tiempo... tiempos) {
        String query = buildQueryString(new ArrayList<>(), Arrays.asList(tiempos));
        return baseUrl + "/search?" + query;
    }

    public String search(List<String> verbs, List<Tiempo> tiempos) {
        String query = buildQueryString(verbs, tiempos);
        return baseUrl + "/search?" + query;
    }

    public String findByVerb(String verb) {
        return baseUrl + "/verb/" + verb;

    }

    public String findByTiempo(Tiempo tiempo) {
        return baseUrl + "/tiempo/" + tiempo.getValue();
    }

    public String findByVerbAndTiempo(String verb, Tiempo tiempo) {
        return baseUrl + "/search?verb=" + verb + "&tiempo=" + tiempo.getValue();

    }

    public String findByVerbSuffixAndTiempo(String suffix, Tiempo tiempo) {
        return baseUrl + "/suffix/" + suffix + "?tiempo=" + tiempo.getValue();

    }

    public String findByVerbSuffixAndTiempos(String suffix, List<Tiempo> tiempos) {
        return buildQueryString(tiempos.toArray(new Tiempo[tiempos.size()]));
    }


    public String findByTiempoAndVerbNotIn(Tiempo tiempo, List<String> verbs) {
        String query = buildQueryString(verbs.toArray(new String[verbs.size()]));
        return baseUrl + "/excludeVerb/" + tiempo.getValue() + "?" + query;
    }


    private String buildQueryString(Tiempo[] tiempos) {
        StringBuilder sb = new StringBuilder();
        Arrays.asList(tiempos).forEach(t -> sb.append("tiempo=").append(t.getValue()).append("&"));
        return sb.toString();
    }

    private String buildQueryString(String[] verbs) {
        StringBuilder sb = new StringBuilder();
        Arrays.asList(verbs).forEach(v -> sb.append("verb=").append(v).append("&"));
        return sb.toString();
    }

    private String buildQueryString(List<String> verbs, List<Tiempo> tiempos) {
        StringBuilder sb = new StringBuilder();
        verbs.forEach(v -> sb.append("verb=").append(v).append("&"));
        tiempos.forEach(t -> sb.append("tiempo=").append(t.getValue()).append("&"));
        return sb.toString();
    }


}