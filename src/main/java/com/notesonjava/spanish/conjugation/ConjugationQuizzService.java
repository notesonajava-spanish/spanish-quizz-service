package com.notesonjava.spanish.conjugation;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.notesonjava.spanish.DocumentAdapter;
import com.notesonjava.spanish.model.Conjugation;
import com.notesonjava.spanish.model.ConjugationAnswer;
import com.notesonjava.spanish.model.Persona;
import com.notesonjava.spanish.model.Tiempo;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbConjugation;
import com.notesonjava.spanish.model.WordStats;
import com.notesonjava.spanish.verbs.VerbClient;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class ConjugationQuizzService {
	
	private static final List<Tiempo> tiempos = Arrays.asList(Tiempo.IndicativoPresente, Tiempo.IndicativoPreteritoPerfectoSimple, Tiempo.IndicativoPreteritoimperfecto, Tiempo.ImperativoPresente, Tiempo.IndicativoFuturo, Tiempo.IndicativoCondicional, Tiempo.Participio, Tiempo.Gerundio, Tiempo.GerundioCompuesto, Tiempo.IndicativoFuturoPerfecto);
	
	private ConjugationClient client;

	private VerbClient verbClient;
	
	
	private ConjugationAnswerRepository conjugationAnswerRepo;
	
	public List<String> getImportantVerbs() {
		return Arrays.asList("estar", "ser", "tener", "poner", "poder", "hacer", "dar", "caer", "haber", "traer", "caber", "prestar", "llevar", "llover", "llorar");
	}
	
	public List<Conjugation> generateRandom(String username, int size) throws JsonParseException, JsonMappingException, IOException{
		Random rand = new Random();
		int index = rand.nextInt(tiempos.size());
		Tiempo tiempo = tiempos.get(index);
		
		List<Verb> verbs = verbClient.getPopularVerbs(500);
		Collections.shuffle(verbs);		
		List<String> query = Flowable.fromIterable(verbs)
					.map(v -> v.getSpanish())
					.toList().blockingGet();

		return generate(username, query, tiempo, size);
	}
	
	public List<Conjugation> generate(String username, List<Persona> personas, int size) throws JsonParseException, JsonMappingException, IOException {
	
		Flowable<String> verbFlow = Flowable.fromIterable(verbClient.getPopularVerbs(500)).map(v -> v.getSpanish());
		
		Single<WordStats<Tiempo>> tiempoFlow = conjugationTiempoReport(username).reduce( (s1, s2) -> (s1.getScore() > s2.getScore())? s2: s1).toSingle();
		List<String> verbs = verbFlow.toList().blockingGet();
		Tiempo selected = tiempoFlow.blockingGet().getItem();
		
		List<Conjugation> list = Flowable.fromIterable(client.getConjugations(verbs, selected))
				.map(vc -> convert(vc))
				.flatMap(vcList -> Flowable.fromIterable(vcList))
				.filter(c -> personas.contains(c.getPersona()))
				.toList().blockingGet();
			
		Collections.shuffle(list);
		if(list.size()>size){
			return list.subList(0, size);
		}
		return list;
	}
	
	public List<Conjugation> generate(String username, List<String> verbs, Tiempo tiempo, int size) throws JsonParseException, JsonMappingException, IOException {
		
		List<Conjugation> list = Flowable.fromIterable(client.getConjugations(verbs, tiempo))
				.map(vc -> convert(vc))
				.flatMap(vcList -> Flowable.fromIterable(vcList))
				.toList().blockingGet();
		
		Collections.shuffle(list);
		if(list.size() > size){
			return list.subList(0, size);
		}
		return list;
	}
	
	public Flowable<WordStats<Conjugation>> conjugationReport(String username){
		Map<Conjugation, Collection<ConjugationAnswer>> answers = Flowable.fromPublisher(conjugationAnswerRepo.findByUsername(username))
							.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
							.toMultimap(answer -> answer.getConjugation())
							.blockingGet();
		
		return Flowable.fromIterable(answers.entrySet())
				.map(entry -> createStats(entry.getKey(), entry.getValue()));
	}

	public Flowable<WordStats<String>> verbReport(String username){
		Map<String, Collection<ConjugationAnswer>> answers = Flowable.fromPublisher(conjugationAnswerRepo.findByUsername(username))
							.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
							.toMultimap(answer -> answer.getConjugation().getVerb())
							.blockingGet();
		
		return Flowable.fromIterable(answers.entrySet())
				.map(entry -> createStats(entry.getKey(), entry.getValue()));
	}
	
	
	public Flowable<WordStats<Tiempo>> conjugationTiempoReport(String username){
		
		Map<Tiempo, Collection<ConjugationAnswer>> answers = Flowable.fromPublisher(conjugationAnswerRepo.findByUsername(username))
				.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
				.toMultimap(answer -> answer.getConjugation().getTiempo())
				.blockingGet();
		
		return Flowable.fromIterable(answers.entrySet())
			.map(entry -> createStats(entry.getKey(), entry.getValue()));
		
	}
	
	public Flowable<WordStats<LocalDate>> getLastWeekReport(String username){		
		ZonedDateTime refDate= ZonedDateTime.now().minusWeeks(1).truncatedTo(ChronoUnit.DAYS);
		Map<LocalDate, Collection<ConjugationAnswer>> map  = Flowable.fromPublisher(conjugationAnswerRepo.findByAnswerTimeAfterAndUsername(refDate, username))
						.map(doc -> DocumentAdapter.toConjugationAnswer(doc))
						.toMultimap(ConjugationAnswer::getAnswerDay)
						.blockingGet();
		return Flowable.fromIterable(map.entrySet()).map(entry -> createStats(entry.getKey(), entry.getValue()));					
	}
	
	private <T> WordStats<T>  createStats(T key, Collection<ConjugationAnswer> answers) {
		WordStats<T> current = new WordStats<>(key);
		answers.forEach(answer -> {
			current.addAttempt();
			if(answer.getScore() == 1) {
				current.addValid();
			}
		});
		return current;
	}
	
	private List<Conjugation> convert(VerbConjugation vc) {
		List<Conjugation> conjugations = new ArrayList<>();
		vc.getConjugations().forEach(c -> {
			Conjugation conjugation = new Conjugation();
			conjugation.setVerb(vc.getVerb());
			conjugation.setTiempo(vc.getTiempo());
			conjugation.setWord(c.getWord());
			conjugation.setPersona(c.getPersona());
			
			conjugations.add(conjugation);
		});
		return conjugations;
	}
	
}