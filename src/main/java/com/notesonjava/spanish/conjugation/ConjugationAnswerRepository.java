package com.notesonjava.spanish.conjugation;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.bson.BsonDateTime;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.reactivestreams.Publisher;

import com.mongodb.client.model.Filters;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.mongodb.reactivestreams.client.Success;
import com.notesonjava.spanish.EditingSubscriber;

/*
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.notesonjava.mongo.reactive.AsyncAdapter;
*/
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class ConjugationAnswerRepository {

	private static final String COLLECTION_NAME = "conjugation_answers";
	
	private MongoDatabase database;

	public Publisher<Document> findByUsername(String username){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		Bson filter = Filters.eq("username", username);
		return collection.find(filter);
	}
	
	public Publisher<Document> findByAnswerTimeAfterAndUsername(ZonedDateTime time, String username){
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		BsonDateTime refTime = new BsonDateTime(time.toInstant().toEpochMilli());
		Bson filter = Filters.and(Filters.eq("username", username), Filters.gte("answerTime", refTime));
		return collection.find(filter);	
	}
	
	public Publisher<Success> save(Document doc) {
		MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);
		log.info(doc.toJson());
		Publisher<Success> publisher =  collection.insertOne(doc);
		publisher.subscribe(new EditingSubscriber());
		return publisher;
	}
	
}
