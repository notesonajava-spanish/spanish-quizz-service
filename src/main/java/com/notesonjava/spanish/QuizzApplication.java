package com.notesonjava.spanish;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoDatabase;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.auth.QuizzAccessManager;
import com.notesonjava.spanish.auth.UserHandler;
import com.notesonjava.spanish.conjugation.ConjugationAnswerRepository;
import com.notesonjava.spanish.conjugation.ConjugationClient;
import com.notesonjava.spanish.conjugation.ConjugationQuizzController;
import com.notesonjava.spanish.conjugation.ConjugationQuizzService;
import com.notesonjava.spanish.conjugation.ConjugationUrlBuilder;
import com.notesonjava.spanish.verbs.VerbAnswerRepository;
import com.notesonjava.spanish.verbs.VerbClient;
import com.notesonjava.spanish.verbs.VerbQuizzController;
import com.notesonjava.spanish.verbs.VerbQuizzService;
import com.notesonjava.spanish.verbs.VerbUrlBuilder;

import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuizzApplication {

	public static String remoteConnectionString(String user, String password, String host, String db) {
		return "mongodb+srv://"+user+":"+password+"@"+host+"/"+db;	
	}
	
	public static String localConnectionString(String host, int port) {
		return "mongodb://"+host+":"+port;
	}

	public static void main(String[] args) throws IOException {
		PropertyMap prop = PropertyLoader.loadProperties();
		String portValue = prop.get("server.port").orElse("8080");
		String verbServiceUrl = prop.get("verb.service.url").orElse("http://localhost:8081");
		String conjugationServiceUrl = prop.get("conjugation.service.url").orElse("http://localhost:8082");
		
		
		String mongoPort = prop.get("mongo.port").orElse("27017");
		String mongoHost = prop.get("mongo.host").orElse("localhost");
		String mongoDb = prop.get("mongo.db").orElse("answers");
		String authUserUrl = prop.get("auth.user.url").orElse("https://fmottard.auth0.com/userinfo");
		int dbPort = Integer.parseInt(mongoPort);
		
		MongoClient client = MongoClients.create(localConnectionString(mongoHost, dbPort));
		MongoDatabase database = client.getDatabase(mongoDb);
		
		VerbAnswerRepository verbRepo = new VerbAnswerRepository(database);
		ConjugationAnswerRepository conjugationRepo = new ConjugationAnswerRepository(database);
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		VerbUrlBuilder verbUrlBuilder = new VerbUrlBuilder(verbServiceUrl);
		VerbClient verbClient = new VerbClient(verbUrlBuilder, mapper);
		VerbQuizzService verbService = new VerbQuizzService(verbClient,verbRepo);
		UserHandler userHandler = new UserHandler();
		
		VerbQuizzController verbController = new VerbQuizzController(verbService, verbRepo, userHandler);
		
		ConjugationUrlBuilder conjugationUrlBuilder = new ConjugationUrlBuilder(conjugationServiceUrl);
		ConjugationClient conjugationClient = new ConjugationClient(conjugationUrlBuilder, mapper);
		ConjugationQuizzService conjugationService = new ConjugationQuizzService(conjugationClient, verbClient, conjugationRepo);
		ConjugationQuizzController conjugationController = new ConjugationQuizzController(conjugationService, conjugationRepo, mapper, userHandler);
		
		
		Javalin app = Javalin.create()
				.enableCorsForAllOrigins()
				.port(Integer.parseInt(portValue));
		
		app.defaultContentType("application/json;charset=utf-8");
		app.requestLogger((ctx, timeMs) -> {
		    System.out.println(ctx.method() + " "  + ctx.path() + " took " + timeMs + " ms");
		    // prints "GET /hello took 4.5 ms"
		});
		
		app.get("/ping", ctx -> {
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User: " + user.get());
			} else {
				log.info("Missing user");
			}
			ctx.result("I am alive : " +user.orElse("Missing User"));
		});
		
		QuizzAccessManager accessManager = new QuizzAccessManager(authUserUrl, mapper);
		
		app.accessManager((handler, ctx, permittedRoles) -> accessManager.manage(handler, ctx, permittedRoles));	
		verbController.init(app);
		conjugationController.init(app);
		
		app.start();
	}
}

