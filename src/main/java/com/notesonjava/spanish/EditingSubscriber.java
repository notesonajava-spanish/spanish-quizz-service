package com.notesonjava.spanish;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import com.mongodb.reactivestreams.client.Success;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EditingSubscriber implements Subscriber<Success> {
	@Override
	public void onSubscribe(Subscription s) {
		log.info("Subscribed");	
		s.request(1);
	}

	@Override
	public void onNext(Success t) {
	}

	@Override
	public void onError(Throwable t) {
		log.info("Error : " + t);
	}

	@Override
	public void onComplete() {
		log.info("Completed");
	}
}