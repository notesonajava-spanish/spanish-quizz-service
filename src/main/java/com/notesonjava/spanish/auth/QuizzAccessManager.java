package com.notesonjava.spanish.auth;

import java.time.ZonedDateTime;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.javalin.Context;
import io.javalin.Handler;
import io.javalin.security.AccessManager;
import io.javalin.security.Role;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@RequiredArgsConstructor
public class QuizzAccessManager implements AccessManager {
	@NonNull
	private String authUserUrl;
	@NonNull
	private ObjectMapper mapper;
	
	private ConcurrentHashMap<String, CachedToken> tokenManager = new ConcurrentHashMap<>();
	
	
	@Override
	public void manage(Handler handler, Context ctx, Set<Role> permittedRoles) throws Exception {
		if(!ctx.method().equalsIgnoreCase("options")) {
			String token = ctx.header("Authorization");
			if(token == null) {
				ctx.status(401).result("Missing Authorization token");
			} else {
				CachedToken cached = tokenManager.get(token);
				if(cached != null && !cached.isExpired()) {
					ctx.attribute("Quizz-User", cached.getUser().getSub());
					handler.handle(ctx);
				} else {
					HttpResponse response = HttpRequest.get(authUserUrl).header("Authorization", token).send();				
					log.info(response.toString());
					if(response.statusCode() == 200) {
						String userJson = response.bodyText();
						UserInfo user = mapper.readValue(userJson, UserInfo.class);
						if(user != null) {
							tokenManager.put(token, new CachedToken(token, ZonedDateTime.now().plusMinutes(5), user));
							log.info(user.toString());
							ctx.attribute("Quizz-User", user.getSub());
							handler.handle(ctx);
						} else {
							log.error("Missing user in userinfo request");
						}
					} else {
						ctx.status(401).result("Invalid Authorization token");
					}
				}
			}
		} else {
			handler.handle(ctx);
		}
	}
}


@AllArgsConstructor
@Data
class CachedToken {
	private String token;
	private ZonedDateTime expiration;
	private UserInfo user;
	
	public boolean isExpired() {
		return ZonedDateTime.now().isAfter(expiration);
	}
}