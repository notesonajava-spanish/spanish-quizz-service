package com.notesonjava.spanish.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class UserInfo {
	
	private String sub;
	
}
/*	{	
		"sub":"auth0|5bec6dda5c45c255a8544a40",
		"nickname":"test",
		"name":"test@dummy.com",
		"picture":"https://s.gravatar.com/avatar/5fc1088ac5fb8cdb0c08e18a0bd3f309?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fte.png",
		"updated_at":"2018-11-23T22:28:02.077Z",
		"http://guate-bet/country":"Guatemala",
		"http://guate-bet/timezone":"America/Guatemala",
		"http://guate-bet/nit":"123456",
		"http://guate-bet/color":"blue"}
	
}*/
