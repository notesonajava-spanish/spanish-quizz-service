FROM adoptopenjdk/openjdk11:jre-11.0.2.9-alpine


ADD build/libs/spanish-quizz-service.jar app.jar


ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /app.jar" ]



